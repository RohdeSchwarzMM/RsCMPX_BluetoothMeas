BhRate
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ABORt:BLUetooth:MEASurement<Instance>:BHRate
	single: STOP:BLUetooth:MEASurement<Instance>:BHRate
	single: INITiate:BLUetooth:MEASurement<Instance>:BHRate

.. code-block:: python

	ABORt:BLUetooth:MEASurement<Instance>:BHRate
	STOP:BLUetooth:MEASurement<Instance>:BHRate
	INITiate:BLUetooth:MEASurement<Instance>:BHRate



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.BhRate.BhRateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.bhRate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_BhRate_InputSignal.rst
	Bluetooth_Measurement_BhRate_Modulation.rst
	Bluetooth_Measurement_BhRate_Pencoding.rst
	Bluetooth_Measurement_BhRate_PowerVsTime.rst
	Bluetooth_Measurement_BhRate_Sgacp.rst
	Bluetooth_Measurement_BhRate_State.rst
	Bluetooth_Measurement_BhRate_Trace.rst