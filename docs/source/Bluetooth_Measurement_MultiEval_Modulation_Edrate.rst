Edrate
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Modulation.Edrate.EdrateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.multiEval.modulation.edrate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_MultiEval_Modulation_Edrate_Average.rst
	Bluetooth_Measurement_MultiEval_Modulation_Edrate_Current.rst
	Bluetooth_Measurement_MultiEval_Modulation_Edrate_Maximum.rst
	Bluetooth_Measurement_MultiEval_Modulation_Edrate_StandardDev.rst