Ptype
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:BHRate:ISIGnal:ADETected:PTYPe

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:BHRate:ISIGnal:ADETected:PTYPe



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.BhRate.InputSignal.Adetected.Ptype.PtypeCls
	:members:
	:undoc-members:
	:noindex: