Measurement
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:BLUetooth:MEASurement<Instance>

.. code-block:: python

	ROUTe:BLUetooth:MEASurement<Instance>



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Route.Bluetooth.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.bluetooth.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Bluetooth_Measurement_RfSettings.rst
	Route_Bluetooth_Measurement_Scenario.rst