Scount
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDRP:SCOunt:PVTime
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDRP:SCOunt:SACP
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDRP:SCOunt:MODulation

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:HDRP:SCOunt:PVTime
	CONFigure:BLUetooth:MEASurement<Instance>:HDRP:SCOunt:SACP
	CONFigure:BLUetooth:MEASurement<Instance>:HDRP:SCOunt:MODulation



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.Hdrp.Scount.ScountCls
	:members:
	:undoc-members:
	:noindex: