Nmode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:MMODe:NMODe:LENergy

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:MMODe:NMODe:LENergy



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.RfSettings.Mmode.Nmode.NmodeCls
	:members:
	:undoc-members:
	:noindex: