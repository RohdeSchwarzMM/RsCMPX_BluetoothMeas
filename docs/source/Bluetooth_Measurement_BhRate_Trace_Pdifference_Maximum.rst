Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:BHRate:TRACe:PDIFference:MAXimum
	single: FETCh:BLUetooth:MEASurement<Instance>:BHRate:TRACe:PDIFference:MAXimum

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:BHRate:TRACe:PDIFference:MAXimum
	FETCh:BLUetooth:MEASurement<Instance>:BHRate:TRACe:PDIFference:MAXimum



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.BhRate.Trace.Pdifference.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: