Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDR:TRACe:SGACp:MAXimum
	single: FETCh:BLUetooth:MEASurement<Instance>:HDR:TRACe:SGACp:MAXimum

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDR:TRACe:SGACp:MAXimum
	FETCh:BLUetooth:MEASurement<Instance>:HDR:TRACe:SGACp:MAXimum



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.Hdr.Trace.Sgacp.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: