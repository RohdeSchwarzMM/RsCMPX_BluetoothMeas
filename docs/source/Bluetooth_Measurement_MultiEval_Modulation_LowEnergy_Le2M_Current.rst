Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LE2M:CURRent
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LE2M:CURRent
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LE2M:CURRent

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LE2M:CURRent
	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LE2M:CURRent
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:LENergy:LE2M:CURRent



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Modulation.LowEnergy.Le2M.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: