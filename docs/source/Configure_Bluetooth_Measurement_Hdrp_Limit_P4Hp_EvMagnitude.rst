EvMagnitude
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDRP:LIMit:P4HP:EVMagnitude:OFFSet

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:HDRP:LIMit:P4HP:EVMagnitude:OFFSet



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.Hdrp.Limit.P4Hp.EvMagnitude.EvMagnitudeCls
	:members:
	:undoc-members:
	:noindex: