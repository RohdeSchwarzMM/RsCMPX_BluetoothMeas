Sacp
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Sacp.SacpCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.multiEval.sacp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_MultiEval_Sacp_Brate.rst
	Bluetooth_Measurement_MultiEval_Sacp_LowEnergy.rst
	Bluetooth_Measurement_MultiEval_Sacp_Nmode.rst
	Bluetooth_Measurement_MultiEval_Sacp_Qhsl.rst