PowerVsTime
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.ListPy.Segment.PowerVsTime.PowerVsTimeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.multiEval.listPy.segment.powerVsTime.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_MultiEval_ListPy_Segment_PowerVsTime_Average.rst
	Bluetooth_Measurement_MultiEval_ListPy_Segment_PowerVsTime_Current.rst
	Bluetooth_Measurement_MultiEval_ListPy_Segment_PowerVsTime_Maximum.rst
	Bluetooth_Measurement_MultiEval_ListPy_Segment_PowerVsTime_Minimum.rst