BdAddress
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:BDADdress:QHSL
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:BDADdress

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:BDADdress:QHSL
	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:BDADdress



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.InputSignal.BdAddress.BdAddressCls
	:members:
	:undoc-members:
	:noindex: