LrStart
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:LRSTart

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:LRSTart



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.RfSettings.LrStart.LrStartCls
	:members:
	:undoc-members:
	:noindex: