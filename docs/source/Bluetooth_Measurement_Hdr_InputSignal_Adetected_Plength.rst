Plength
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:HDR:ISIGnal:ADETected:PLENgth

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:HDR:ISIGnal:ADETected:PLENgth



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.Hdr.InputSignal.Adetected.Plength.PlengthCls
	:members:
	:undoc-members:
	:noindex: