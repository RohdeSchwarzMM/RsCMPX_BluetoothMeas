Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:BHRate:PVTime:AVERage
	single: FETCh:BLUetooth:MEASurement<Instance>:BHRate:PVTime:AVERage
	single: CALCulate:BLUetooth:MEASurement<Instance>:BHRate:PVTime:AVERage

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:BHRate:PVTime:AVERage
	FETCh:BLUetooth:MEASurement<Instance>:BHRate:PVTime:AVERage
	CALCulate:BLUetooth:MEASurement<Instance>:BHRate:PVTime:AVERage



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.BhRate.PowerVsTime.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: