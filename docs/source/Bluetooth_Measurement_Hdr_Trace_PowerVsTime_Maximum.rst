Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDR:TRACe:PVTime:MAXimum
	single: FETCh:BLUetooth:MEASurement<Instance>:HDR:TRACe:PVTime:MAXimum

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDR:TRACe:PVTime:MAXimum
	FETCh:BLUetooth:MEASurement<Instance>:HDR:TRACe:PVTime:MAXimum



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.Hdr.Trace.PowerVsTime.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: