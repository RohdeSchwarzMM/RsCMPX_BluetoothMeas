P2H
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:BHRate:LIMit:P2H:DEVM
	single: CONFigure:BLUetooth:MEASurement<Instance>:BHRate:LIMit:P2H:SGACp

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:BHRate:LIMit:P2H:DEVM
	CONFigure:BLUetooth:MEASurement<Instance>:BHRate:LIMit:P2H:SGACp



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.BhRate.Limit.P2H.P2HCls
	:members:
	:undoc-members:
	:noindex: