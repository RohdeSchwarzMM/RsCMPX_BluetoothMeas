Sgacp
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Trace.Sgacp.SgacpCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.multiEval.trace.sgacp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_MultiEval_Trace_Sgacp_Average.rst
	Bluetooth_Measurement_MultiEval_Trace_Sgacp_Current.rst
	Bluetooth_Measurement_MultiEval_Trace_Sgacp_Maximum.rst
	Bluetooth_Measurement_MultiEval_Trace_Sgacp_Ptx.rst