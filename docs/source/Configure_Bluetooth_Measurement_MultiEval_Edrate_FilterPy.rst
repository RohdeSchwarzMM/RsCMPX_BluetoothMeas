FilterPy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:EDRate:FILTer:BWIDth

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:EDRate:FILTer:BWIDth



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.MultiEval.Edrate.FilterPy.FilterPyCls
	:members:
	:undoc-members:
	:noindex: