Measurement
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_BhRate.rst
	Bluetooth_Measurement_DtMode.rst
	Bluetooth_Measurement_Hdr.rst
	Bluetooth_Measurement_Hdrp.rst
	Bluetooth_Measurement_InputSignal.rst
	Bluetooth_Measurement_MultiEval.rst
	Bluetooth_Measurement_RxQuality.rst
	Bluetooth_Measurement_Trx.rst