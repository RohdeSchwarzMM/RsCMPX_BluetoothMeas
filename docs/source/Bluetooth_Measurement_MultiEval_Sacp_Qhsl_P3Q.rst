P3Q
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:P3Q
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:P3Q
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:P3Q

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:P3Q
	READ:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:P3Q
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:P3Q



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Sacp.Qhsl.P3Q.P3QCls
	:members:
	:undoc-members:
	:noindex: