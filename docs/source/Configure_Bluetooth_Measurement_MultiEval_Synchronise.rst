Synchronise
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SYNChronise

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SYNChronise



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.MultiEval.Synchronise.SynchroniseCls
	:members:
	:undoc-members:
	:noindex: