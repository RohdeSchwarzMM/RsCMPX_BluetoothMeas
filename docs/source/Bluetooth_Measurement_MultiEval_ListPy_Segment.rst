Segment<Segment>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: S1 .. S128
	rc = driver.bluetooth.measurement.multiEval.listPy.segment.repcap_segment_get()
	driver.bluetooth.measurement.multiEval.listPy.segment.repcap_segment_set(repcap.Segment.S1)





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.ListPy.Segment.SegmentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.multiEval.listPy.segment.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_MultiEval_ListPy_Segment_Modulation.rst
	Bluetooth_Measurement_MultiEval_ListPy_Segment_Pencoding.rst
	Bluetooth_Measurement_MultiEval_ListPy_Segment_PowerVsTime.rst
	Bluetooth_Measurement_MultiEval_ListPy_Segment_Sacp.rst
	Bluetooth_Measurement_MultiEval_ListPy_Segment_SoBw.rst