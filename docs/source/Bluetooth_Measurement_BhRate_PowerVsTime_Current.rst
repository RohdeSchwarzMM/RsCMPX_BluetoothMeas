Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:BHRate:PVTime:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:BHRate:PVTime:CURRent
	single: CALCulate:BLUetooth:MEASurement<Instance>:BHRate:PVTime:CURRent

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:BHRate:PVTime:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:BHRate:PVTime:CURRent
	CALCulate:BLUetooth:MEASurement<Instance>:BHRate:PVTime:CURRent



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.BhRate.PowerVsTime.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: