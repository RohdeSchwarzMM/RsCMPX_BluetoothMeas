Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:BHRate:TRACe:PDIFference:AVERage
	single: FETCh:BLUetooth:MEASurement<Instance>:BHRate:TRACe:PDIFference:AVERage

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:BHRate:TRACe:PDIFference:AVERage
	FETCh:BLUetooth:MEASurement<Instance>:BHRate:TRACe:PDIFference:AVERage



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.BhRate.Trace.Pdifference.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: