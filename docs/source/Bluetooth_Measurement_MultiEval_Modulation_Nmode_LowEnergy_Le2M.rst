Le2M
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Modulation.Nmode.LowEnergy.Le2M.Le2MCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.multiEval.modulation.nmode.lowEnergy.le2M.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_MultiEval_Modulation_Nmode_LowEnergy_Le2M_Average.rst
	Bluetooth_Measurement_MultiEval_Modulation_Nmode_LowEnergy_Le2M_Current.rst
	Bluetooth_Measurement_MultiEval_Modulation_Nmode_LowEnergy_Le2M_Maximum.rst
	Bluetooth_Measurement_MultiEval_Modulation_Nmode_LowEnergy_Le2M_Minimum.rst
	Bluetooth_Measurement_MultiEval_Modulation_Nmode_LowEnergy_Le2M_StandardDev.rst
	Bluetooth_Measurement_MultiEval_Modulation_Nmode_LowEnergy_Le2M_Xmaximum.rst
	Bluetooth_Measurement_MultiEval_Modulation_Nmode_LowEnergy_Le2M_Xminimum.rst
	Bluetooth_Measurement_MultiEval_Modulation_Nmode_LowEnergy_Le2M_YieldPy.rst