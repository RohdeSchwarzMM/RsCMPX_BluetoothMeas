RfSettings
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:BLUetooth:MEASurement<Instance>:RFSettings:CONNector

.. code-block:: python

	ROUTe:BLUetooth:MEASurement<Instance>:RFSettings:CONNector



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Route.Bluetooth.Measurement.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex: