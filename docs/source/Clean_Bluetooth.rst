Bluetooth
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Clean.Bluetooth.BluetoothCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.clean.bluetooth.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Clean_Bluetooth_Measurement.rst