Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:FDEViation:MAXimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:FDEViation:MAXimum

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:FDEViation:MAXimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:FDEViation:MAXimum



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Trace.Fdeviation.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: