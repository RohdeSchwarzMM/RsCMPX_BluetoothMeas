Adetected
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.Hdrp.InputSignal.Adetected.AdetectedCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.hdrp.inputSignal.adetected.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_Hdrp_InputSignal_Adetected_Pcoding.rst
	Bluetooth_Measurement_Hdrp_InputSignal_Adetected_Plength.rst