State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:RXQuality:STATe

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:RXQuality:STATe



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.RxQuality.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.rxQuality.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_RxQuality_State_All.rst