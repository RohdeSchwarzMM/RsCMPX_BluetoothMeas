Le1M
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:CTE:LENergy:LE1M:FDRift

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:CTE:LENergy:LE1M:FDRift



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.MultiEval.Limit.Cte.LowEnergy.Le1M.Le1MCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.bluetooth.measurement.multiEval.limit.cte.lowEnergy.le1M.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Bluetooth_Measurement_MultiEval_Limit_Cte_LowEnergy_Le1M_Foffset.rst
	Configure_Bluetooth_Measurement_MultiEval_Limit_Cte_LowEnergy_Le1M_Pdeviation.rst