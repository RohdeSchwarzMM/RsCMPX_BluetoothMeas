LowEnergy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:LENergy:SYNWord
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:LENergy:PHY

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:LENergy:SYNWord
	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:LENergy:PHY



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.InputSignal.LowEnergy.LowEnergyCls
	:members:
	:undoc-members:
	:noindex: