DtMode
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.InputSignal.DtMode.DtModeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.bluetooth.measurement.inputSignal.dtMode.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Bluetooth_Measurement_InputSignal_DtMode_Pattern.rst
	Configure_Bluetooth_Measurement_InputSignal_DtMode_Plength.rst
	Configure_Bluetooth_Measurement_InputSignal_DtMode_RxQuality.rst