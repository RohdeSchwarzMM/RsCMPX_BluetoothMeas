SpotCheck
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:SPOTcheck:LEVel

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:SPOTcheck:LEVel



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.RxQuality.SpotCheck.SpotCheckCls
	:members:
	:undoc-members:
	:noindex: