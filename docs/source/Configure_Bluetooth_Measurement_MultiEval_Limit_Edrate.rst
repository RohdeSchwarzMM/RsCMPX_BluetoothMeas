Edrate
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:EDRate:PVTime
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:EDRate:FSTability

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:EDRate:PVTime
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:EDRate:FSTability



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.MultiEval.Limit.Edrate.EdrateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.bluetooth.measurement.multiEval.limit.edrate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Bluetooth_Measurement_MultiEval_Limit_Edrate_Dpsk.rst
	Configure_Bluetooth_Measurement_MultiEval_Limit_Edrate_Dqpsk.rst
	Configure_Bluetooth_Measurement_MultiEval_Limit_Edrate_Pencoding.rst