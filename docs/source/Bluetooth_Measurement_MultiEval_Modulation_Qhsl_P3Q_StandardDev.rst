StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P3Q:SDEViation
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P3Q:SDEViation
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P3Q:SDEViation

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P3Q:SDEViation
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P3Q:SDEViation
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P3Q:SDEViation



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Modulation.Qhsl.P3Q.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: