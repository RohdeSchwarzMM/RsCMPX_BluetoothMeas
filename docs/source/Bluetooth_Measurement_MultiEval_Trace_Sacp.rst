Sacp
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Trace.Sacp.SacpCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.multiEval.trace.sacp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_MultiEval_Trace_Sacp_Average.rst
	Bluetooth_Measurement_MultiEval_Trace_Sacp_Current.rst
	Bluetooth_Measurement_MultiEval_Trace_Sacp_Maximum.rst
	Bluetooth_Measurement_MultiEval_Trace_Sacp_Ptx.rst