Lap
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:LAP:QHSL
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:LAP

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:LAP:QHSL
	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:LAP



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.InputSignal.Lap.LapCls
	:members:
	:undoc-members:
	:noindex: