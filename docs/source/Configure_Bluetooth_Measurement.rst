Measurement
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:HWINterface
	single: CONFigure:BLUetooth:MEASurement<Instance>:CPRotocol
	single: CONFigure:BLUetooth:MEASurement<Instance>:GDELay
	single: CONFigure:BLUetooth:MEASurement<Instance>:CFILter
	single: CONFigure:BLUetooth:MEASurement<Instance>:OTHReshold

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:HWINterface
	CONFigure:BLUetooth:MEASurement<Instance>:CPRotocol
	CONFigure:BLUetooth:MEASurement<Instance>:GDELay
	CONFigure:BLUetooth:MEASurement<Instance>:CFILter
	CONFigure:BLUetooth:MEASurement<Instance>:OTHReshold



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.bluetooth.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Bluetooth_Measurement_BhRate.rst
	Configure_Bluetooth_Measurement_ComSettings.rst
	Configure_Bluetooth_Measurement_Display.rst
	Configure_Bluetooth_Measurement_DtMode.rst
	Configure_Bluetooth_Measurement_Hdr.rst
	Configure_Bluetooth_Measurement_Hdrp.rst
	Configure_Bluetooth_Measurement_InputSignal.rst
	Configure_Bluetooth_Measurement_MultiEval.rst
	Configure_Bluetooth_Measurement_RfSettings.rst
	Configure_Bluetooth_Measurement_RxQuality.rst
	Configure_Bluetooth_Measurement_Trx.rst