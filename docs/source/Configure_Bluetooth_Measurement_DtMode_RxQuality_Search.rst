Search
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:STARtlevel
	single: CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:STEP

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:STARtlevel
	CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:STEP



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.DtMode.RxQuality.Search.SearchCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.bluetooth.measurement.dtMode.rxQuality.search.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Bluetooth_Measurement_DtMode_RxQuality_Search_Limit.rst
	Configure_Bluetooth_Measurement_DtMode_RxQuality_Search_Packets.rst
	Configure_Bluetooth_Measurement_DtMode_RxQuality_Search_Rintegrity.rst