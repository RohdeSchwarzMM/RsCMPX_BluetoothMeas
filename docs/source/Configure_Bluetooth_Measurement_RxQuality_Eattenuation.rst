Eattenuation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:EATTenuation:OUTPut

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:EATTenuation:OUTPut



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.RxQuality.Eattenuation.EattenuationCls
	:members:
	:undoc-members:
	:noindex: