Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:BHRate:TRACe:SGACp:MAXimum
	single: FETCh:BLUetooth:MEASurement<Instance>:BHRate:TRACe:SGACp:MAXimum

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:BHRate:TRACe:SGACp:MAXimum
	FETCh:BLUetooth:MEASurement<Instance>:BHRate:TRACe:SGACp:MAXimum



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.BhRate.Trace.Sgacp.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: