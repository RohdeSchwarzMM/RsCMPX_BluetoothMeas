Limit
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:SGACp

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:SGACp



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.MultiEval.Limit.LimitCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.bluetooth.measurement.multiEval.limit.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Bluetooth_Measurement_MultiEval_Limit_Brate.rst
	Configure_Bluetooth_Measurement_MultiEval_Limit_Cte.rst
	Configure_Bluetooth_Measurement_MultiEval_Limit_Edrate.rst
	Configure_Bluetooth_Measurement_MultiEval_Limit_Frange.rst
	Configure_Bluetooth_Measurement_MultiEval_Limit_LowEnergy.rst
	Configure_Bluetooth_Measurement_MultiEval_Limit_PowerVsTime.rst
	Configure_Bluetooth_Measurement_MultiEval_Limit_Qhsl.rst
	Configure_Bluetooth_Measurement_MultiEval_Limit_Sacp.rst
	Configure_Bluetooth_Measurement_MultiEval_Limit_SoBw.rst