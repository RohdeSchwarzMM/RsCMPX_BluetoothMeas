Hdrp
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDRP:MOEXception
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDRP:SCONdition
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDRP:REPetition

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:HDRP:MOEXception
	CONFigure:BLUetooth:MEASurement<Instance>:HDRP:SCONdition
	CONFigure:BLUetooth:MEASurement<Instance>:HDRP:REPetition



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.Hdrp.HdrpCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.bluetooth.measurement.hdrp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Bluetooth_Measurement_Hdrp_InputSignal.rst
	Configure_Bluetooth_Measurement_Hdrp_Limit.rst
	Configure_Bluetooth_Measurement_Hdrp_Result.rst
	Configure_Bluetooth_Measurement_Hdrp_Sacp.rst
	Configure_Bluetooth_Measurement_Hdrp_Scount.rst