RxQuality
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INITiate:BLUetooth:MEASurement<Instance>:RXQuality
	single: STOP:BLUetooth:MEASurement<Instance>:RXQuality
	single: ABORt:BLUetooth:MEASurement<Instance>:RXQuality

.. code-block:: python

	INITiate:BLUetooth:MEASurement<Instance>:RXQuality
	STOP:BLUetooth:MEASurement<Instance>:RXQuality
	ABORt:BLUetooth:MEASurement<Instance>:RXQuality



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.RxQuality.RxQualityCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.rxQuality.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_RxQuality_Adetected.rst
	Bluetooth_Measurement_RxQuality_Per.rst
	Bluetooth_Measurement_RxQuality_Sensitivity.rst
	Bluetooth_Measurement_RxQuality_SpotCheck.rst
	Bluetooth_Measurement_RxQuality_State.rst