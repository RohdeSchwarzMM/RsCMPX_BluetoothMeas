Faccuracy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:BRATe:FACCuracy

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:BRATe:FACCuracy



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.MultiEval.Limit.Brate.Faccuracy.FaccuracyCls
	:members:
	:undoc-members:
	:noindex: