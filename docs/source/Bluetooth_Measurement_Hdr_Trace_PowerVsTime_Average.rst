Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDR:TRACe:PVTime:AVERage
	single: FETCh:BLUetooth:MEASurement<Instance>:HDR:TRACe:PVTime:AVERage

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDR:TRACe:PVTime:AVERage
	FETCh:BLUetooth:MEASurement<Instance>:HDR:TRACe:PVTime:AVERage



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.Hdr.Trace.PowerVsTime.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: