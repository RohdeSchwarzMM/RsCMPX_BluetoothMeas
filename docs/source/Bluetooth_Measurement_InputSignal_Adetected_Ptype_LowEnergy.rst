LowEnergy
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.InputSignal.Adetected.Ptype.LowEnergy.LowEnergyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.inputSignal.adetected.ptype.lowEnergy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_InputSignal_Adetected_Ptype_LowEnergy_Le1M.rst
	Bluetooth_Measurement_InputSignal_Adetected_Ptype_LowEnergy_Le2M.rst
	Bluetooth_Measurement_InputSignal_Adetected_Ptype_LowEnergy_Lrange.rst