Uap
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:UAP:QHSL
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:UAP

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:UAP:QHSL
	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:UAP



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.InputSignal.Uap.UapCls
	:members:
	:undoc-members:
	:noindex: