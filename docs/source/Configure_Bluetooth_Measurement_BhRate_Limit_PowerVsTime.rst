PowerVsTime
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:BHRate:LIMit:PVTime

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:BHRate:LIMit:PVTime



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.BhRate.Limit.PowerVsTime.PowerVsTimeCls
	:members:
	:undoc-members:
	:noindex: