Scount
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:BHRate:SCOunt:PVTime
	single: CONFigure:BLUetooth:MEASurement<Instance>:BHRate:SCOunt:SGACp
	single: CONFigure:BLUetooth:MEASurement<Instance>:BHRate:SCOunt:MODulation
	single: CONFigure:BLUetooth:MEASurement<Instance>:BHRate:SCOunt:PENCoding

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:BHRate:SCOunt:PVTime
	CONFigure:BLUetooth:MEASurement<Instance>:BHRate:SCOunt:SGACp
	CONFigure:BLUetooth:MEASurement<Instance>:BHRate:SCOunt:MODulation
	CONFigure:BLUetooth:MEASurement<Instance>:BHRate:SCOunt:PENCoding



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.BhRate.Scount.ScountCls
	:members:
	:undoc-members:
	:noindex: