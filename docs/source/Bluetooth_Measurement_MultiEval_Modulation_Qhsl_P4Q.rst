P4Q
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Modulation.Qhsl.P4Q.P4QCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.multiEval.modulation.qhsl.p4Q.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_MultiEval_Modulation_Qhsl_P4Q_Average.rst
	Bluetooth_Measurement_MultiEval_Modulation_Qhsl_P4Q_Current.rst
	Bluetooth_Measurement_MultiEval_Modulation_Qhsl_P4Q_Maximum.rst
	Bluetooth_Measurement_MultiEval_Modulation_Qhsl_P4Q_StandardDev.rst