Plength
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:BHRate:ISIGnal:ADETected:PLENgth

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:BHRate:ISIGnal:ADETected:PLENgth



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.BhRate.InputSignal.Adetected.Plength.PlengthCls
	:members:
	:undoc-members:
	:noindex: