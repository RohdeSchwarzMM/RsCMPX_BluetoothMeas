RxPackets
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:RXQuality:PER:RXPackets

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:RXQuality:PER:RXPackets



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.RxQuality.Per.RxPackets.RxPacketsCls
	:members:
	:undoc-members:
	:noindex: