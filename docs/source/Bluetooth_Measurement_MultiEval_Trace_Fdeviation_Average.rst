Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:FDEViation:AVERage
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:FDEViation:AVERage

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:FDEViation:AVERage
	READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:FDEViation:AVERage



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Trace.Fdeviation.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: