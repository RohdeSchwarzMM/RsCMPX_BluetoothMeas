SpotCheck
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:RXQuality:SPOTcheck

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:RXQuality:SPOTcheck



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.RxQuality.SpotCheck.SpotCheckCls
	:members:
	:undoc-members:
	:noindex: