Eattenuation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:EATTenuation:OUTPut

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:EATTenuation:OUTPut



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.DtMode.RxQuality.Eattenuation.EattenuationCls
	:members:
	:undoc-members:
	:noindex: