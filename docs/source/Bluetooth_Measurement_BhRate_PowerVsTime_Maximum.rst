Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:BHRate:PVTime:MAXimum
	single: FETCh:BLUetooth:MEASurement<Instance>:BHRate:PVTime:MAXimum
	single: CALCulate:BLUetooth:MEASurement<Instance>:BHRate:PVTime:MAXimum

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:BHRate:PVTime:MAXimum
	FETCh:BLUetooth:MEASurement<Instance>:BHRate:PVTime:MAXimum
	CALCulate:BLUetooth:MEASurement<Instance>:BHRate:PVTime:MAXimum



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.BhRate.PowerVsTime.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: