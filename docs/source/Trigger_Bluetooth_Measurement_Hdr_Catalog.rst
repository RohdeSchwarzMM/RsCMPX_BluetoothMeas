Catalog
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:BLUetooth:MEASurement<Instance>:HDR:CATalog:SOURce

.. code-block:: python

	TRIGger:BLUetooth:MEASurement<Instance>:HDR:CATalog:SOURce



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Trigger.Bluetooth.Measurement.Hdr.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: