Trace
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.multiEval.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_MultiEval_Trace_DevMagnitude.rst
	Bluetooth_Measurement_MultiEval_Trace_Fdeviation.rst
	Bluetooth_Measurement_MultiEval_Trace_Frange.rst
	Bluetooth_Measurement_MultiEval_Trace_IqAbs.rst
	Bluetooth_Measurement_MultiEval_Trace_IqDifference.rst
	Bluetooth_Measurement_MultiEval_Trace_IqError.rst
	Bluetooth_Measurement_MultiEval_Trace_Pdeviation.rst
	Bluetooth_Measurement_MultiEval_Trace_Pdifference.rst
	Bluetooth_Measurement_MultiEval_Trace_PowerVsTime.rst
	Bluetooth_Measurement_MultiEval_Trace_Sacp.rst
	Bluetooth_Measurement_MultiEval_Trace_Sgacp.rst
	Bluetooth_Measurement_MultiEval_Trace_SoBw.rst
	Bluetooth_Measurement_MultiEval_Trace_Spower.rst