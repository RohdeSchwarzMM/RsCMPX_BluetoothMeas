LowEnergy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PACKets:LENergy:LE1M
	single: CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PACKets:LENergy:LE2M
	single: CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PACKets:LENergy:LRANge

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PACKets:LENergy:LE1M
	CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PACKets:LENergy:LE2M
	CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:SEARch:PACKets:LENergy:LRANge



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.DtMode.RxQuality.Search.Packets.LowEnergy.LowEnergyCls
	:members:
	:undoc-members:
	:noindex: