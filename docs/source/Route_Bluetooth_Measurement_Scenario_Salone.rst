Salone
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:BLUetooth:MEASurement<Instance>:SCENario:SALone

.. code-block:: python

	ROUTe:BLUetooth:MEASurement<Instance>:SCENario:SALone



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Route.Bluetooth.Measurement.Scenario.Salone.SaloneCls
	:members:
	:undoc-members:
	:noindex: