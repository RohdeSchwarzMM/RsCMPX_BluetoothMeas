Adetected
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.InputSignal.Adetected.AdetectedCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.inputSignal.adetected.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_InputSignal_Adetected_Aaddress.rst
	Bluetooth_Measurement_InputSignal_Adetected_Coding.rst
	Bluetooth_Measurement_InputSignal_Adetected_Cte.rst
	Bluetooth_Measurement_InputSignal_Adetected_NoSlots.rst
	Bluetooth_Measurement_InputSignal_Adetected_Pattern.rst
	Bluetooth_Measurement_InputSignal_Adetected_PduType.rst
	Bluetooth_Measurement_InputSignal_Adetected_Plength.rst
	Bluetooth_Measurement_InputSignal_Adetected_Ptype.rst
	Bluetooth_Measurement_InputSignal_Adetected_Qhsl.rst