LowEnergy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PLENgth:LENergy[:LE1M]
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PLENgth:LENergy:LRANge
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PLENgth:LENergy:LE2M

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PLENgth:LENergy[:LE1M]
	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PLENgth:LENergy:LRANge
	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PLENgth:LENergy:LE2M



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.InputSignal.Plength.LowEnergy.LowEnergyCls
	:members:
	:undoc-members:
	:noindex: