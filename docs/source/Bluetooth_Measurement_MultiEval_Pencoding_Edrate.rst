Edrate
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Pencoding.Edrate.EdrateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.multiEval.pencoding.edrate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_MultiEval_Pencoding_Edrate_Current.rst