Per
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:PER:LEVel
	single: CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:PER:TXPackets

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:PER:LEVel
	CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:PER:TXPackets



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.RxQuality.Per.PerCls
	:members:
	:undoc-members:
	:noindex: