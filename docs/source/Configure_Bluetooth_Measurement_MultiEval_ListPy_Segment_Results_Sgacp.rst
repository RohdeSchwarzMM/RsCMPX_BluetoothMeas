Sgacp
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:RESults:SGACp

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:RESults:SGACp



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.MultiEval.ListPy.Segment.Results.Sgacp.SgacpCls
	:members:
	:undoc-members:
	:noindex: