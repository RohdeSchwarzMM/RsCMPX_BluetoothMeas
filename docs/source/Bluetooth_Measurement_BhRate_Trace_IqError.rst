IqError
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:BHRate:TRACe:IQERr
	single: FETCh:BLUetooth:MEASurement<Instance>:BHRate:TRACe:IQERr

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:BHRate:TRACe:IQERr
	FETCh:BLUetooth:MEASurement<Instance>:BHRate:TRACe:IQERr



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.BhRate.Trace.IqError.IqErrorCls
	:members:
	:undoc-members:
	:noindex: