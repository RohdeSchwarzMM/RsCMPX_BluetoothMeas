PowerVsTime
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.Hdr.PowerVsTime.PowerVsTimeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.hdr.powerVsTime.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_Hdr_PowerVsTime_Average.rst
	Bluetooth_Measurement_Hdr_PowerVsTime_Current.rst
	Bluetooth_Measurement_Hdr_PowerVsTime_Maximum.rst
	Bluetooth_Measurement_Hdr_PowerVsTime_Minimum.rst