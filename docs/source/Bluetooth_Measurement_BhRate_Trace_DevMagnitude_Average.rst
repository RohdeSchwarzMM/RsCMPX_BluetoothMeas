Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:BHRate:TRACe:DEVMagnitude:AVERage
	single: FETCh:BLUetooth:MEASurement<Instance>:BHRate:TRACe:DEVMagnitude:AVERage

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:BHRate:TRACe:DEVMagnitude:AVERage
	FETCh:BLUetooth:MEASurement<Instance>:BHRate:TRACe:DEVMagnitude:AVERage



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.BhRate.Trace.DevMagnitude.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: