Hdr
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDR:MOEXception
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDR:SCONdition
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDR:REPetition

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:HDR:MOEXception
	CONFigure:BLUetooth:MEASurement<Instance>:HDR:SCONdition
	CONFigure:BLUetooth:MEASurement<Instance>:HDR:REPetition



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.Hdr.HdrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.bluetooth.measurement.hdr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Bluetooth_Measurement_Hdr_InputSignal.rst
	Configure_Bluetooth_Measurement_Hdr_Limit.rst
	Configure_Bluetooth_Measurement_Hdr_Result.rst
	Configure_Bluetooth_Measurement_Hdr_Scount.rst
	Configure_Bluetooth_Measurement_Hdr_Sgacp.rst