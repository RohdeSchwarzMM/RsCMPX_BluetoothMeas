Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:EDRate:AVERage
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:EDRate:AVERage
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:EDRate:AVERage

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:EDRate:AVERage
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:EDRate:AVERage
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:EDRate:AVERage



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Modulation.Edrate.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: