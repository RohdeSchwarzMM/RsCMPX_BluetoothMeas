Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDR:PVTime:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:HDR:PVTime:CURRent
	single: CALCulate:BLUetooth:MEASurement<Instance>:HDR:PVTime:CURRent

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDR:PVTime:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:HDR:PVTime:CURRent
	CALCulate:BLUetooth:MEASurement<Instance>:HDR:PVTime:CURRent



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.Hdr.PowerVsTime.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: