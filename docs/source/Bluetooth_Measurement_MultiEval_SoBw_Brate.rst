Brate
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.SoBw.Brate.BrateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.multiEval.soBw.brate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_MultiEval_SoBw_Brate_Maximum.rst