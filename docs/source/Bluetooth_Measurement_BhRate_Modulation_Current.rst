Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:BHRate:MODulation:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:BHRate:MODulation:CURRent
	single: CALCulate:BLUetooth:MEASurement<Instance>:BHRate:MODulation:CURRent

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:BHRate:MODulation:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:BHRate:MODulation:CURRent
	CALCulate:BLUetooth:MEASurement<Instance>:BHRate:MODulation:CURRent



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.BhRate.Modulation.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: