Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P5Q:MAXimum
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P5Q:MAXimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P5Q:MAXimum

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P5Q:MAXimum
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P5Q:MAXimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:QHSL:P5Q:MAXimum



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Modulation.Qhsl.P5Q.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: