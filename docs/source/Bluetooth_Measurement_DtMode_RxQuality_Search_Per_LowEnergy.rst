LowEnergy
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.DtMode.RxQuality.Search.Per.LowEnergy.LowEnergyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.dtMode.rxQuality.search.per.lowEnergy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_DtMode_RxQuality_Search_Per_LowEnergy_Le1M.rst
	Bluetooth_Measurement_DtMode_RxQuality_Search_Per_LowEnergy_Le2M.rst
	Bluetooth_Measurement_DtMode_RxQuality_Search_Per_LowEnergy_Lrange.rst