Edrate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:NOSLots:EDRate

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:NOSLots:EDRate



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.InputSignal.Adetected.NoSlots.Edrate.EdrateCls
	:members:
	:undoc-members:
	:noindex: