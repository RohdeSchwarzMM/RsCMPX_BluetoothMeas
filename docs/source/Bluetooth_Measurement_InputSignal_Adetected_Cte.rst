Cte
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.InputSignal.Adetected.Cte.CteCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.inputSignal.adetected.cte.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_InputSignal_Adetected_Cte_LowEnergy.rst
	Bluetooth_Measurement_InputSignal_Adetected_Cte_Qhsl.rst