LowEnergy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:PER:PACKets:LENergy:LE1M
	single: CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:PER:PACKets:LENergy:LE2M
	single: CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:PER:PACKets:LENergy:LRANge

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:PER:PACKets:LENergy:LE1M
	CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:PER:PACKets:LENergy:LE2M
	CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:PER:PACKets:LENergy:LRANge



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.DtMode.RxQuality.Per.Packets.LowEnergy.LowEnergyCls
	:members:
	:undoc-members:
	:noindex: