Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDR:TRACe:PVTime:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:HDR:TRACe:PVTime:CURRent

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDR:TRACe:PVTime:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:HDR:TRACe:PVTime:CURRent



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.Hdr.Trace.PowerVsTime.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: