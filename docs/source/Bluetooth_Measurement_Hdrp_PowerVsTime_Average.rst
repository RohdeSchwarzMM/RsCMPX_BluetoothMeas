Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDRP:PVTime:AVERage
	single: FETCh:BLUetooth:MEASurement<Instance>:HDRP:PVTime:AVERage
	single: CALCulate:BLUetooth:MEASurement<Instance>:HDRP:PVTime:AVERage

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDRP:PVTime:AVERage
	FETCh:BLUetooth:MEASurement<Instance>:HDRP:PVTime:AVERage
	CALCulate:BLUetooth:MEASurement<Instance>:HDRP:PVTime:AVERage



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.Hdrp.PowerVsTime.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: