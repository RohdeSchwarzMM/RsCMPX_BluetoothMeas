Sgacp
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.Hdr.Trace.Sgacp.SgacpCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.hdr.trace.sgacp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_Hdr_Trace_Sgacp_Average.rst
	Bluetooth_Measurement_Hdr_Trace_Sgacp_Current.rst
	Bluetooth_Measurement_Hdr_Trace_Sgacp_Maximum.rst
	Bluetooth_Measurement_Hdr_Trace_Sgacp_Ptx.rst