FilterPy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LENergy[:LE1M]:FILTer:BWIDth

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LENergy[:LE1M]:FILTer:BWIDth



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.MultiEval.LowEnergy.Le1M.FilterPy.FilterPyCls
	:members:
	:undoc-members:
	:noindex: