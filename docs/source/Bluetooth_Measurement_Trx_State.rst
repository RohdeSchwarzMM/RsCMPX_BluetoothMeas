State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:TRX:STATe

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:TRX:STATe



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.Trx.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.trx.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_Trx_State_All.rst