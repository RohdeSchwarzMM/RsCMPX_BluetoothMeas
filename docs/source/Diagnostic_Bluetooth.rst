Bluetooth
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Diagnostic.Bluetooth.BluetoothCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.bluetooth.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Bluetooth_Measurement.rst
	Diagnostic_Bluetooth_Synchronise.rst