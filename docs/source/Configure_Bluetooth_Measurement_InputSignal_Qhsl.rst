Qhsl
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:QHSL:PHY

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:QHSL:PHY



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.InputSignal.Qhsl.QhslCls
	:members:
	:undoc-members:
	:noindex: