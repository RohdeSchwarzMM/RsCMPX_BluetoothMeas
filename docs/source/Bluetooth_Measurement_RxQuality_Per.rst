Per
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:RXQuality:PER

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:RXQuality:PER



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.RxQuality.Per.PerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.rxQuality.per.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_RxQuality_Per_RxPackets.rst