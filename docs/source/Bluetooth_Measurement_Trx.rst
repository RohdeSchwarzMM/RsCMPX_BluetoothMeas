Trx
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INITiate:BLUetooth:MEASurement<Instance>:TRX
	single: STOP:BLUetooth:MEASurement<Instance>:TRX
	single: ABORt:BLUetooth:MEASurement<Instance>:TRX

.. code-block:: python

	INITiate:BLUetooth:MEASurement<Instance>:TRX
	STOP:BLUetooth:MEASurement<Instance>:TRX
	ABORt:BLUetooth:MEASurement<Instance>:TRX



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.Trx.TrxCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.trx.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_Trx_Acp.rst
	Bluetooth_Measurement_Trx_Modulation.rst
	Bluetooth_Measurement_Trx_Power.rst
	Bluetooth_Measurement_Trx_Spot.rst
	Bluetooth_Measurement_Trx_State.rst