Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDR:TRACe:DEVMagnitude:MAXimum
	single: FETCh:BLUetooth:MEASurement<Instance>:HDR:TRACe:DEVMagnitude:MAXimum

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDR:TRACe:DEVMagnitude:MAXimum
	FETCh:BLUetooth:MEASurement<Instance>:HDR:TRACe:DEVMagnitude:MAXimum



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.Hdr.Trace.DevMagnitude.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: