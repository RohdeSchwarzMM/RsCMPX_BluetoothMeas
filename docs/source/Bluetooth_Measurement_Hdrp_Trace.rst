Trace
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.Hdrp.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.hdrp.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_Hdrp_Trace_EvMagnitude.rst
	Bluetooth_Measurement_Hdrp_Trace_IqAbs.rst
	Bluetooth_Measurement_Hdrp_Trace_IqOffset.rst
	Bluetooth_Measurement_Hdrp_Trace_PowerVsTime.rst
	Bluetooth_Measurement_Hdrp_Trace_Sacp.rst