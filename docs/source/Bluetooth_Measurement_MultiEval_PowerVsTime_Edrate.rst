Edrate
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.PowerVsTime.Edrate.EdrateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.multiEval.powerVsTime.edrate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_MultiEval_PowerVsTime_Edrate_Average.rst
	Bluetooth_Measurement_MultiEval_PowerVsTime_Edrate_Current.rst
	Bluetooth_Measurement_MultiEval_PowerVsTime_Edrate_Maximum.rst
	Bluetooth_Measurement_MultiEval_PowerVsTime_Edrate_Minimum.rst