P3Q
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:CTE:QHSL:P3Q:TYPE
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:CTE:QHSL:P3Q:UNITs

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:CTE:QHSL:P3Q:TYPE
	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:CTE:QHSL:P3Q:UNITs



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.InputSignal.Cte.Qhsl.P3Q.P3QCls
	:members:
	:undoc-members:
	:noindex: