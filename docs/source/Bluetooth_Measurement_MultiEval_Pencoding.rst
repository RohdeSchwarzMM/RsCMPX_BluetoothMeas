Pencoding
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Pencoding.PencodingCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.multiEval.pencoding.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_MultiEval_Pencoding_Edrate.rst
	Bluetooth_Measurement_MultiEval_Pencoding_Ssequence.rst