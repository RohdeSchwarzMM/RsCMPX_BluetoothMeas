Pdifference
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.Hdr.Trace.Pdifference.PdifferenceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.hdr.trace.pdifference.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_Hdr_Trace_Pdifference_Average.rst
	Bluetooth_Measurement_Hdr_Trace_Pdifference_Current.rst
	Bluetooth_Measurement_Hdr_Trace_Pdifference_Maximum.rst