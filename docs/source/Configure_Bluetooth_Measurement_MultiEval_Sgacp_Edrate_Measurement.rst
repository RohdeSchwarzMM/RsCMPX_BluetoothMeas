Measurement
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SGACp:EDRate:MEASurement:MODE

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SGACp:EDRate:MEASurement:MODE



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.MultiEval.Sgacp.Edrate.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex: