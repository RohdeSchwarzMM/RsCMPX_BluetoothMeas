Spot
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:TRX:SPOT

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:TRX:SPOT



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.Trx.Spot.SpotCls
	:members:
	:undoc-members:
	:noindex: