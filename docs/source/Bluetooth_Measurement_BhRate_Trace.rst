Trace
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.BhRate.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.bhRate.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_BhRate_Trace_DevMagnitude.rst
	Bluetooth_Measurement_BhRate_Trace_IqAbs.rst
	Bluetooth_Measurement_BhRate_Trace_IqDifference.rst
	Bluetooth_Measurement_BhRate_Trace_IqError.rst
	Bluetooth_Measurement_BhRate_Trace_Pdifference.rst
	Bluetooth_Measurement_BhRate_Trace_PowerVsTime.rst
	Bluetooth_Measurement_BhRate_Trace_Sgacp.rst