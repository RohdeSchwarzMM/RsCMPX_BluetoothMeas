Scenario
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ROUTe:BLUetooth:MEASurement<Instance>:SCENario:CSPath
	single: ROUTe:BLUetooth:MEASurement<Instance>:SCENario

.. code-block:: python

	ROUTe:BLUetooth:MEASurement<Instance>:SCENario:CSPath
	ROUTe:BLUetooth:MEASurement<Instance>:SCENario



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Route.Bluetooth.Measurement.Scenario.ScenarioCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.bluetooth.measurement.scenario.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Bluetooth_Measurement_Scenario_MaProtocol.rst
	Route_Bluetooth_Measurement_Scenario_Salone.rst