Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDR:PENCoding:SSEQuence:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:HDR:PENCoding:SSEQuence:CURRent
	single: CALCulate:BLUetooth:MEASurement<Instance>:HDR:PENCoding:SSEQuence:CURRent

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDR:PENCoding:SSEQuence:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:HDR:PENCoding:SSEQuence:CURRent
	CALCulate:BLUetooth:MEASurement<Instance>:HDR:PENCoding:SSEQuence:CURRent



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.Hdr.Pencoding.Ssequence.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: