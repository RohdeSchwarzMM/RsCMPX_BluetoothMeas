BhRate
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:BLUetooth:MEASurement<Instance>:BHRate:SOURce
	single: TRIGger:BLUetooth:MEASurement<Instance>:BHRate:THReshold
	single: TRIGger:BLUetooth:MEASurement<Instance>:BHRate:TOUT

.. code-block:: python

	TRIGger:BLUetooth:MEASurement<Instance>:BHRate:SOURce
	TRIGger:BLUetooth:MEASurement<Instance>:BHRate:THReshold
	TRIGger:BLUetooth:MEASurement<Instance>:BHRate:TOUT



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Trigger.Bluetooth.Measurement.BhRate.BhRateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.bluetooth.measurement.bhRate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Bluetooth_Measurement_BhRate_Catalog.rst