Pencoding
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:BHRate:LIMit:PENCoding

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:BHRate:LIMit:PENCoding



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.BhRate.Limit.Pencoding.PencodingCls
	:members:
	:undoc-members:
	:noindex: