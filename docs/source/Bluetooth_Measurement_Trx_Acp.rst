Acp
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:TRX:ACP

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:TRX:ACP



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.Trx.Acp.AcpCls
	:members:
	:undoc-members:
	:noindex: