Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LRANge:AVERage
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LRANge:AVERage
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LRANge:AVERage

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LRANge:AVERage
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LRANge:AVERage
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LRANge:AVERage



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.PowerVsTime.LowEnergy.Lrange.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: