Brate
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.PowerVsTime.Brate.BrateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.multiEval.powerVsTime.brate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_MultiEval_PowerVsTime_Brate_Average.rst
	Bluetooth_Measurement_MultiEval_PowerVsTime_Brate_Current.rst
	Bluetooth_Measurement_MultiEval_PowerVsTime_Brate_Maximum.rst
	Bluetooth_Measurement_MultiEval_PowerVsTime_Brate_Minimum.rst