Classic
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Modulation.Nmode.Classic.ClassicCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.multiEval.modulation.nmode.classic.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_MultiEval_Modulation_Nmode_Classic_Average.rst
	Bluetooth_Measurement_MultiEval_Modulation_Nmode_Classic_Current.rst
	Bluetooth_Measurement_MultiEval_Modulation_Nmode_Classic_Maximum.rst
	Bluetooth_Measurement_MultiEval_Modulation_Nmode_Classic_Minimum.rst
	Bluetooth_Measurement_MultiEval_Modulation_Nmode_Classic_StandardDev.rst
	Bluetooth_Measurement_MultiEval_Modulation_Nmode_Classic_Xmaximum.rst
	Bluetooth_Measurement_MultiEval_Modulation_Nmode_Classic_Xminimum.rst