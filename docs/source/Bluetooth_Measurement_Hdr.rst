Hdr
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ABORt:BLUetooth:MEASurement<Instance>:HDR
	single: STOP:BLUetooth:MEASurement<Instance>:HDR
	single: INITiate:BLUetooth:MEASurement<Instance>:HDR

.. code-block:: python

	ABORt:BLUetooth:MEASurement<Instance>:HDR
	STOP:BLUetooth:MEASurement<Instance>:HDR
	INITiate:BLUetooth:MEASurement<Instance>:HDR



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.Hdr.HdrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.hdr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_Hdr_InputSignal.rst
	Bluetooth_Measurement_Hdr_Modulation.rst
	Bluetooth_Measurement_Hdr_Pencoding.rst
	Bluetooth_Measurement_Hdr_PowerVsTime.rst
	Bluetooth_Measurement_Hdr_Sgacp.rst
	Bluetooth_Measurement_Hdr_State.rst
	Bluetooth_Measurement_Hdr_Trace.rst