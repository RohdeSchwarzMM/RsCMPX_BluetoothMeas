Ptx
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:BHRate:TRACe:SGACp[:PTX]
	single: FETCh:BLUetooth:MEASurement<Instance>:BHRate:TRACe:SGACp[:PTX]

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:BHRate:TRACe:SGACp[:PTX]
	FETCh:BLUetooth:MEASurement<Instance>:BHRate:TRACe:SGACp[:PTX]



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.BhRate.Trace.Sgacp.Ptx.PtxCls
	:members:
	:undoc-members:
	:noindex: