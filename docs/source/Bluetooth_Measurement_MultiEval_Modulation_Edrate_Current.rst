Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:EDRate:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:EDRate:CURRent
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:EDRate:CURRent

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:EDRate:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:EDRate:CURRent
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:EDRate:CURRent



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Modulation.Edrate.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.multiEval.modulation.edrate.current.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_MultiEval_Modulation_Edrate_Current_Extended.rst