Plength
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDRP:ISIGnal:PLENgth

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:HDRP:ISIGnal:PLENgth



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.Hdrp.InputSignal.Plength.PlengthCls
	:members:
	:undoc-members:
	:noindex: