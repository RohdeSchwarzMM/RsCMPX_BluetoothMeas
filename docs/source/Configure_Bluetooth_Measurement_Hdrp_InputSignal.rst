InputSignal
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDRP:ISIGnal:PHY
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDRP:ISIGnal:PCODing
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDRP:ISIGnal:DMODe

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:HDRP:ISIGnal:PHY
	CONFigure:BLUetooth:MEASurement<Instance>:HDRP:ISIGnal:PCODing
	CONFigure:BLUetooth:MEASurement<Instance>:HDRP:ISIGnal:DMODe



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.Hdrp.InputSignal.InputSignalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.bluetooth.measurement.hdrp.inputSignal.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Bluetooth_Measurement_Hdrp_InputSignal_Plength.rst