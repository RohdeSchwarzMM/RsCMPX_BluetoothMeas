P5Q
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:CTE:QHSL:P5Q:TYPE
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:CTE:QHSL:P5Q:UNITs

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:CTE:QHSL:P5Q:TYPE
	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:CTE:QHSL:P5Q:UNITs



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.InputSignal.Cte.Qhsl.P5Q.P5QCls
	:members:
	:undoc-members:
	:noindex: