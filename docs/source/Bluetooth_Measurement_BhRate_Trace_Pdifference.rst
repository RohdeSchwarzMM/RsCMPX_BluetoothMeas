Pdifference
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.BhRate.Trace.Pdifference.PdifferenceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.bhRate.trace.pdifference.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_BhRate_Trace_Pdifference_Average.rst
	Bluetooth_Measurement_BhRate_Trace_Pdifference_Current.rst
	Bluetooth_Measurement_BhRate_Trace_Pdifference_Maximum.rst