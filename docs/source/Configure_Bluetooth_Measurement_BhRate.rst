BhRate
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:BHRate:MOEXception
	single: CONFigure:BLUetooth:MEASurement<Instance>:BHRate:SCONdition
	single: CONFigure:BLUetooth:MEASurement<Instance>:BHRate:REPetition

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:BHRate:MOEXception
	CONFigure:BLUetooth:MEASurement<Instance>:BHRate:SCONdition
	CONFigure:BLUetooth:MEASurement<Instance>:BHRate:REPetition



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.BhRate.BhRateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.bluetooth.measurement.bhRate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Bluetooth_Measurement_BhRate_InputSignal.rst
	Configure_Bluetooth_Measurement_BhRate_Limit.rst
	Configure_Bluetooth_Measurement_BhRate_Result.rst
	Configure_Bluetooth_Measurement_BhRate_Scount.rst
	Configure_Bluetooth_Measurement_BhRate_Sgacp.rst