Qhsl
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.PowerVsTime.Qhsl.QhslCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.multiEval.powerVsTime.qhsl.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_MultiEval_PowerVsTime_Qhsl_P2Q.rst
	Bluetooth_Measurement_MultiEval_PowerVsTime_Qhsl_P3Q.rst
	Bluetooth_Measurement_MultiEval_PowerVsTime_Qhsl_P4Q.rst
	Bluetooth_Measurement_MultiEval_PowerVsTime_Qhsl_P5Q.rst
	Bluetooth_Measurement_MultiEval_PowerVsTime_Qhsl_P6Q.rst