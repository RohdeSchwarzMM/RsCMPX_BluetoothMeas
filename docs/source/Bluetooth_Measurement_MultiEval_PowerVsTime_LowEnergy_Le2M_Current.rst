Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LE2M:CURRent
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LE2M:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LE2M:CURRent

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LE2M:CURRent
	READ:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LE2M:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:PVTime:LENergy:LE2M:CURRent



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.PowerVsTime.LowEnergy.Le2M.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: