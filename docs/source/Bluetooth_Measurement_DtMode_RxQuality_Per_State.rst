State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:PER:STATe

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:PER:STATe



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.DtMode.RxQuality.Per.State.StateCls
	:members:
	:undoc-members:
	:noindex: