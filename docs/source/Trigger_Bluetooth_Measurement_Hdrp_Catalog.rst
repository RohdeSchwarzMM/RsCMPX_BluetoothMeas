Catalog
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:BLUetooth:MEASurement<Instance>:HDRP:CATalog:SOURce

.. code-block:: python

	TRIGger:BLUetooth:MEASurement<Instance>:HDRP:CATalog:SOURce



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Trigger.Bluetooth.Measurement.Hdrp.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: