Le1M
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Modulation.Cte.LowEnergy.Le1M.Le1MCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.multiEval.modulation.cte.lowEnergy.le1M.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_MultiEval_Modulation_Cte_LowEnergy_Le1M_Average.rst
	Bluetooth_Measurement_MultiEval_Modulation_Cte_LowEnergy_Le1M_Current.rst
	Bluetooth_Measurement_MultiEval_Modulation_Cte_LowEnergy_Le1M_Maximum.rst
	Bluetooth_Measurement_MultiEval_Modulation_Cte_LowEnergy_Le1M_StandardDev.rst
	Bluetooth_Measurement_MultiEval_Modulation_Cte_LowEnergy_Le1M_Xmaximum.rst
	Bluetooth_Measurement_MultiEval_Modulation_Cte_LowEnergy_Le1M_Xminimum.rst