Pattern
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PATTern

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PATTern



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.InputSignal.Pattern.PatternCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.bluetooth.measurement.inputSignal.pattern.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Bluetooth_Measurement_InputSignal_Pattern_LowEnergy.rst