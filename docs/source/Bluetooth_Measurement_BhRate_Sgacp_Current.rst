Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:BHRate:SGACp[:CURRent]
	single: FETCh:BLUetooth:MEASurement<Instance>:BHRate:SGACp[:CURRent]
	single: CALCulate:BLUetooth:MEASurement<Instance>:BHRate:SGACp[:CURRent]

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:BHRate:SGACp[:CURRent]
	FETCh:BLUetooth:MEASurement<Instance>:BHRate:SGACp[:CURRent]
	CALCulate:BLUetooth:MEASurement<Instance>:BHRate:SGACp[:CURRent]



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.BhRate.Sgacp.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: