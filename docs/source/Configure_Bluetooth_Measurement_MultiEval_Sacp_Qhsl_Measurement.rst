Measurement
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:MEASurement:MODE

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SACP:QHSL:MEASurement:MODE



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.MultiEval.Sacp.Qhsl.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex: