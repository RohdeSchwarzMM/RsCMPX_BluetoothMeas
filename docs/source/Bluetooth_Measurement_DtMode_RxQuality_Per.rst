Per
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INITiate:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:PER
	single: STOP:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:PER
	single: ABORt:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:PER

.. code-block:: python

	INITiate:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:PER
	STOP:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:PER
	ABORt:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:PER



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.DtMode.RxQuality.Per.PerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.dtMode.rxQuality.per.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_DtMode_RxQuality_Per_LowEnergy.rst
	Bluetooth_Measurement_DtMode_RxQuality_Per_State.rst