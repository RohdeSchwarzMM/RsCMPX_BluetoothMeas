RfControl
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:BLUetooth:MEASurement<Instance>:RFControl:TXENable

.. code-block:: python

	DIAGnostic:BLUetooth:MEASurement<Instance>:RFControl:TXENable



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Diagnostic.Bluetooth.Measurement.RfControl.RfControlCls
	:members:
	:undoc-members:
	:noindex: