StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:BHRate:MODulation:SDEViation
	single: FETCh:BLUetooth:MEASurement<Instance>:BHRate:MODulation:SDEViation
	single: CALCulate:BLUetooth:MEASurement<Instance>:BHRate:MODulation:SDEViation

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:BHRate:MODulation:SDEViation
	FETCh:BLUetooth:MEASurement<Instance>:BHRate:MODulation:SDEViation
	CALCulate:BLUetooth:MEASurement<Instance>:BHRate:MODulation:SDEViation



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.BhRate.Modulation.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: