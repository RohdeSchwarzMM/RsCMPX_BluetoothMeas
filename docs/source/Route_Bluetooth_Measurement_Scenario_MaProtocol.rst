MaProtocol
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:BLUetooth:MEASurement<Instance>:SCENario:MAPRotocol

.. code-block:: python

	ROUTe:BLUetooth:MEASurement<Instance>:SCENario:MAPRotocol



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Route.Bluetooth.Measurement.Scenario.MaProtocol.MaProtocolCls
	:members:
	:undoc-members:
	:noindex: