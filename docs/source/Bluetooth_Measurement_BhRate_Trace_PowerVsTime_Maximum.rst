Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:BHRate:TRACe:PVTime:MAXimum
	single: FETCh:BLUetooth:MEASurement<Instance>:BHRate:TRACe:PVTime:MAXimum

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:BHRate:TRACe:PVTime:MAXimum
	FETCh:BLUetooth:MEASurement<Instance>:BHRate:TRACe:PVTime:MAXimum



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.BhRate.Trace.PowerVsTime.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: