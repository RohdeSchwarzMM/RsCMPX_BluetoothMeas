MultiEval
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: STOP:BLUetooth:MEASurement<Instance>:MEValuation
	single: ABORt:BLUetooth:MEASurement<Instance>:MEValuation
	single: INITiate:BLUetooth:MEASurement<Instance>:MEValuation

.. code-block:: python

	STOP:BLUetooth:MEASurement<Instance>:MEValuation
	ABORt:BLUetooth:MEASurement<Instance>:MEValuation
	INITiate:BLUetooth:MEASurement<Instance>:MEValuation



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_MultiEval_Frange.rst
	Bluetooth_Measurement_MultiEval_ListPy.rst
	Bluetooth_Measurement_MultiEval_Modulation.rst
	Bluetooth_Measurement_MultiEval_Pencoding.rst
	Bluetooth_Measurement_MultiEval_PowerVsTime.rst
	Bluetooth_Measurement_MultiEval_Sacp.rst
	Bluetooth_Measurement_MultiEval_Sgacp.rst
	Bluetooth_Measurement_MultiEval_SoBw.rst
	Bluetooth_Measurement_MultiEval_State.rst
	Bluetooth_Measurement_MultiEval_Trace.rst