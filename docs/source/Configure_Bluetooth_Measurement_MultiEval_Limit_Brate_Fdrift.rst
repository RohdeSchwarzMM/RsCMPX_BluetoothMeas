Fdrift
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:BRATe:FDRift
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:BRATe:FDRift:APACkets

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:BRATe:FDRift
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:LIMit:BRATe:FDRift:APACkets



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.MultiEval.Limit.Brate.Fdrift.FdriftCls
	:members:
	:undoc-members:
	:noindex: