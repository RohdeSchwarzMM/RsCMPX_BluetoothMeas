Measurement
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:BHRate:SGACp:MEASurement:MODE

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:BHRate:SGACp:MEASurement:MODE



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.BhRate.Sgacp.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex: