Pencoding
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDR:LIMit:PENCoding

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:HDR:LIMit:PENCoding



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.Hdr.Limit.Pencoding.PencodingCls
	:members:
	:undoc-members:
	:noindex: