Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SOBW:MAXimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SOBW:MAXimum

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SOBW:MAXimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SOBW:MAXimum



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Trace.SoBw.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: