LowEnergy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PTYPe:LENergy[:LE1M]
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PTYPe:LENergy:LRANge
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PTYPe:LENergy:LE2M

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PTYPe:LENergy[:LE1M]
	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PTYPe:LENergy:LRANge
	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:PTYPe:LENergy:LE2M



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.InputSignal.Ptype.LowEnergy.LowEnergyCls
	:members:
	:undoc-members:
	:noindex: