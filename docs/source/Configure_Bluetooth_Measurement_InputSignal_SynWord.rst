SynWord
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:SYNWord:LENergy

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:SYNWord:LENergy



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.InputSignal.SynWord.SynWordCls
	:members:
	:undoc-members:
	:noindex: