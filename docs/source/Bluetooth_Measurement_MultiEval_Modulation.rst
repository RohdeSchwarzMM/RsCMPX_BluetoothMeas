Modulation
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.multiEval.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_MultiEval_Modulation_Brate.rst
	Bluetooth_Measurement_MultiEval_Modulation_Cte.rst
	Bluetooth_Measurement_MultiEval_Modulation_Edrate.rst
	Bluetooth_Measurement_MultiEval_Modulation_LowEnergy.rst
	Bluetooth_Measurement_MultiEval_Modulation_Nmode.rst
	Bluetooth_Measurement_MultiEval_Modulation_Qhsl.rst