Result
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:BHRate:RESult[:ALL]
	single: CONFigure:BLUetooth:MEASurement<Instance>:BHRate:RESult:PVTime
	single: CONFigure:BLUetooth:MEASurement<Instance>:BHRate:RESult:SGACp
	single: CONFigure:BLUetooth:MEASurement<Instance>:BHRate:RESult:PENCoding
	single: CONFigure:BLUetooth:MEASurement<Instance>:BHRate:RESult:IQERr
	single: CONFigure:BLUetooth:MEASurement<Instance>:BHRate:RESult:IQDiff
	single: CONFigure:BLUetooth:MEASurement<Instance>:BHRate:RESult:IQABsolute
	single: CONFigure:BLUetooth:MEASurement<Instance>:BHRate:RESult:PDIFference
	single: CONFigure:BLUetooth:MEASurement<Instance>:BHRate:RESult:DEVMagnitude
	single: CONFigure:BLUetooth:MEASurement<Instance>:BHRate:RESult:TXSCalar

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:BHRate:RESult[:ALL]
	CONFigure:BLUetooth:MEASurement<Instance>:BHRate:RESult:PVTime
	CONFigure:BLUetooth:MEASurement<Instance>:BHRate:RESult:SGACp
	CONFigure:BLUetooth:MEASurement<Instance>:BHRate:RESult:PENCoding
	CONFigure:BLUetooth:MEASurement<Instance>:BHRate:RESult:IQERr
	CONFigure:BLUetooth:MEASurement<Instance>:BHRate:RESult:IQDiff
	CONFigure:BLUetooth:MEASurement<Instance>:BHRate:RESult:IQABsolute
	CONFigure:BLUetooth:MEASurement<Instance>:BHRate:RESult:PDIFference
	CONFigure:BLUetooth:MEASurement<Instance>:BHRate:RESult:DEVMagnitude
	CONFigure:BLUetooth:MEASurement<Instance>:BHRate:RESult:TXSCalar



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.BhRate.Result.ResultCls
	:members:
	:undoc-members:
	:noindex: