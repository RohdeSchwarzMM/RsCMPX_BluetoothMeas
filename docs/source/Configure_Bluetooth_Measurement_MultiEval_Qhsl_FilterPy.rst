FilterPy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:QHSL:FILTer:BWIDth

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:QHSL:FILTer:BWIDth



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.MultiEval.Qhsl.FilterPy.FilterPyCls
	:members:
	:undoc-members:
	:noindex: