P4Hp
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDRP:LIMit:P4HP:SACP

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:HDRP:LIMit:P4HP:SACP



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.Hdrp.Limit.P4Hp.P4HpCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.bluetooth.measurement.hdrp.limit.p4Hp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Bluetooth_Measurement_Hdrp_Limit_P4Hp_EvMagnitude.rst