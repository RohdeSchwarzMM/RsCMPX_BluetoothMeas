Trace
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.Hdr.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.hdr.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_Hdr_Trace_DevMagnitude.rst
	Bluetooth_Measurement_Hdr_Trace_IqAbs.rst
	Bluetooth_Measurement_Hdr_Trace_IqDifference.rst
	Bluetooth_Measurement_Hdr_Trace_IqError.rst
	Bluetooth_Measurement_Hdr_Trace_Pdifference.rst
	Bluetooth_Measurement_Hdr_Trace_PowerVsTime.rst
	Bluetooth_Measurement_Hdr_Trace_Sgacp.rst