DtMode
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Call.Bluetooth.Measurement.DtMode.DtModeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.call.bluetooth.measurement.dtMode.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Call_Bluetooth_Measurement_DtMode_LowEnergy.rst