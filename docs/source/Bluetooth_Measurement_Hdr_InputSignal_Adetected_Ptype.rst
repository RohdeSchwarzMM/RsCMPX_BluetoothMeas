Ptype
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:HDR:ISIGnal:ADETected:PTYPe

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:HDR:ISIGnal:ADETected:PTYPe



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.Hdr.InputSignal.Adetected.Ptype.PtypeCls
	:members:
	:undoc-members:
	:noindex: