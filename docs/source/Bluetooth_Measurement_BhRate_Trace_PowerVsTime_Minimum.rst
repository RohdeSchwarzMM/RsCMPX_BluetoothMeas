Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:BHRate:TRACe:PVTime:MINimum
	single: FETCh:BLUetooth:MEASurement<Instance>:BHRate:TRACe:PVTime:MINimum

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:BHRate:TRACe:PVTime:MINimum
	FETCh:BLUetooth:MEASurement<Instance>:BHRate:TRACe:PVTime:MINimum



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.BhRate.Trace.PowerVsTime.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: