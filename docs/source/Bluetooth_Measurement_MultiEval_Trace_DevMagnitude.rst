DevMagnitude
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Trace.DevMagnitude.DevMagnitudeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.multiEval.trace.devMagnitude.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_MultiEval_Trace_DevMagnitude_Average.rst
	Bluetooth_Measurement_MultiEval_Trace_DevMagnitude_Current.rst
	Bluetooth_Measurement_MultiEval_Trace_DevMagnitude_Maximum.rst