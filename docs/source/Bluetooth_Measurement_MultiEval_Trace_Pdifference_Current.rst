Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PDIFference:CURRent
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PDIFference:CURRent

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PDIFference:CURRent
	READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PDIFference:CURRent



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Trace.Pdifference.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: