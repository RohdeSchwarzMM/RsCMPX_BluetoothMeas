Mper
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.DtMode.RxQuality.Limit.Mper.MperCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.bluetooth.measurement.dtMode.rxQuality.limit.mper.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Bluetooth_Measurement_DtMode_RxQuality_Limit_Mper_LowEnergy.rst