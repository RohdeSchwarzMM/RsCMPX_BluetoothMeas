Power
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:TRX:POWer

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:TRX:POWer



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.Trx.Power.PowerCls
	:members:
	:undoc-members:
	:noindex: