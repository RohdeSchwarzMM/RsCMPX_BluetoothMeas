Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:HDR:TRACe:PDIFference:AVERage
	single: FETCh:BLUetooth:MEASurement<Instance>:HDR:TRACe:PDIFference:AVERage

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:HDR:TRACe:PDIFference:AVERage
	FETCh:BLUetooth:MEASurement<Instance>:HDR:TRACe:PDIFference:AVERage



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.Hdr.Trace.Pdifference.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: