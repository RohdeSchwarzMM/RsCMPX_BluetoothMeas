Mchannel
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:MCHannel[:CLASsic]
	single: CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:MCHannel:LENergy

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:MCHannel[:CLASsic]
	CONFigure:BLUetooth:MEASurement<Instance>:RFSettings:MCHannel:LENergy



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.RfSettings.Mchannel.MchannelCls
	:members:
	:undoc-members:
	:noindex: