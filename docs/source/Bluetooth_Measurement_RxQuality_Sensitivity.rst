Sensitivity
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:RXQuality:SENSitivity

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:RXQuality:SENSitivity



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.RxQuality.Sensitivity.SensitivityCls
	:members:
	:undoc-members:
	:noindex: