Modulation
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.ListPy.Segment.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.multiEval.listPy.segment.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_MultiEval_ListPy_Segment_Modulation_Average.rst
	Bluetooth_Measurement_MultiEval_ListPy_Segment_Modulation_Current.rst
	Bluetooth_Measurement_MultiEval_ListPy_Segment_Modulation_Maximum.rst
	Bluetooth_Measurement_MultiEval_ListPy_Segment_Modulation_Minimum.rst
	Bluetooth_Measurement_MultiEval_ListPy_Segment_Modulation_StandardDev.rst
	Bluetooth_Measurement_MultiEval_ListPy_Segment_Modulation_Xmaximum.rst
	Bluetooth_Measurement_MultiEval_ListPy_Segment_Modulation_Xminimum.rst