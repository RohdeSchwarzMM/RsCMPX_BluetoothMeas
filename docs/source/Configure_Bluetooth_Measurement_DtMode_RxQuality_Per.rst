Per
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:PER:LEVel

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:DTMode:RXQuality:PER:LEVel



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.DtMode.RxQuality.Per.PerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.bluetooth.measurement.dtMode.rxQuality.per.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Bluetooth_Measurement_DtMode_RxQuality_Per_Packets.rst