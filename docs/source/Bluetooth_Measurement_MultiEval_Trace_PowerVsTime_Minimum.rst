Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PVTime:MINimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PVTime:MINimum

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PVTime:MINimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:PVTime:MINimum



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Trace.PowerVsTime.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: