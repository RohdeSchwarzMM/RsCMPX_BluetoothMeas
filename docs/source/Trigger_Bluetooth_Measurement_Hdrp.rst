Hdrp
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:BLUetooth:MEASurement<Instance>:HDRP:THReshold
	single: TRIGger:BLUetooth:MEASurement<Instance>:HDRP:TOUT
	single: TRIGger:BLUetooth:MEASurement<Instance>:HDRP:SOURce

.. code-block:: python

	TRIGger:BLUetooth:MEASurement<Instance>:HDRP:THReshold
	TRIGger:BLUetooth:MEASurement<Instance>:HDRP:TOUT
	TRIGger:BLUetooth:MEASurement<Instance>:HDRP:SOURce



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Trigger.Bluetooth.Measurement.Hdrp.HdrpCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.bluetooth.measurement.hdrp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Bluetooth_Measurement_Hdrp_Catalog.rst