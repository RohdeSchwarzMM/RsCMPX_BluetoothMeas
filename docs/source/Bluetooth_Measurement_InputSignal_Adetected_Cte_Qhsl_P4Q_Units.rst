Units
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:CTE:QHSL:P4Q:UNITs

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:ISIGnal:ADETected:CTE:QHSL:P4Q:UNITs



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.InputSignal.Adetected.Cte.Qhsl.P4Q.Units.UnitsCls
	:members:
	:undoc-members:
	:noindex: