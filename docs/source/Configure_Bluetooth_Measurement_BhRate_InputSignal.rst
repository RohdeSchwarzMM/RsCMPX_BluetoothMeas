InputSignal
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:BHRate:ISIGnal:PLENgth
	single: CONFigure:BLUetooth:MEASurement<Instance>:BHRate:ISIGnal:PTYPe
	single: CONFigure:BLUetooth:MEASurement<Instance>:BHRate:ISIGnal:NAP
	single: CONFigure:BLUetooth:MEASurement<Instance>:BHRate:ISIGnal:UAP
	single: CONFigure:BLUetooth:MEASurement<Instance>:BHRate:ISIGnal:LAP
	single: CONFigure:BLUetooth:MEASurement<Instance>:BHRate:ISIGnal:BDADdress
	single: CONFigure:BLUetooth:MEASurement<Instance>:BHRate:ISIGnal:ASYNchronize
	single: CONFigure:BLUetooth:MEASurement<Instance>:BHRate:ISIGnal:DMODe

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:BHRate:ISIGnal:PLENgth
	CONFigure:BLUetooth:MEASurement<Instance>:BHRate:ISIGnal:PTYPe
	CONFigure:BLUetooth:MEASurement<Instance>:BHRate:ISIGnal:NAP
	CONFigure:BLUetooth:MEASurement<Instance>:BHRate:ISIGnal:UAP
	CONFigure:BLUetooth:MEASurement<Instance>:BHRate:ISIGnal:LAP
	CONFigure:BLUetooth:MEASurement<Instance>:BHRate:ISIGnal:BDADdress
	CONFigure:BLUetooth:MEASurement<Instance>:BHRate:ISIGnal:ASYNchronize
	CONFigure:BLUetooth:MEASurement<Instance>:BHRate:ISIGnal:DMODe



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.BhRate.InputSignal.InputSignalCls
	:members:
	:undoc-members:
	:noindex: