RsCMPX_BluetoothMeas API Structure
========================================


.. rubric:: Global RepCaps

.. code-block:: python
	
	driver = RsCMPX_BluetoothMeas('TCPIP::192.168.2.101::hislip0')
	# Instance range: Inst1 .. Inst16
	rc = driver.repcap_instance_get()
	driver.repcap_instance_set(repcap.Instance.Inst1)

.. autoclass:: RsCMPX_BluetoothMeas.RsCMPX_BluetoothMeas
	:members:
	:undoc-members:
	:noindex:

.. rubric:: Subgroups

.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth.rst
	Call.rst
	Clean.rst
	Configure.rst
	Diagnostic.rst
	Route.rst
	Sense.rst
	Trigger.rst