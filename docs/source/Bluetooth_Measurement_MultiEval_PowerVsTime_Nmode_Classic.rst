Classic
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.PowerVsTime.Nmode.Classic.ClassicCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.multiEval.powerVsTime.nmode.classic.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_MultiEval_PowerVsTime_Nmode_Classic_Average.rst
	Bluetooth_Measurement_MultiEval_PowerVsTime_Nmode_Classic_Current.rst
	Bluetooth_Measurement_MultiEval_PowerVsTime_Nmode_Classic_Maximum.rst
	Bluetooth_Measurement_MultiEval_PowerVsTime_Nmode_Classic_Minimum.rst