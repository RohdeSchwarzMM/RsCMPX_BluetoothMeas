Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SGACp:MAXimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SGACp:MAXimum

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SGACp:MAXimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:TRACe:SGACp:MAXimum



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Trace.Sgacp.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: