Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:BHRate:TRACe:PDIFference:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:BHRate:TRACe:PDIFference:CURRent

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:BHRate:TRACe:PDIFference:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:BHRate:TRACe:PDIFference:CURRent



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.BhRate.Trace.Pdifference.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: