Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:BHRate:TRACe:SGACp:AVERage
	single: FETCh:BLUetooth:MEASurement<Instance>:BHRate:TRACe:SGACp:AVERage

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:BHRate:TRACe:SGACp:AVERage
	FETCh:BLUetooth:MEASurement<Instance>:BHRate:TRACe:SGACp:AVERage



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.BhRate.Trace.Sgacp.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: