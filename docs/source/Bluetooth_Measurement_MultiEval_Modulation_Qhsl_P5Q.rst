P5Q
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Modulation.Qhsl.P5Q.P5QCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.multiEval.modulation.qhsl.p5Q.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_MultiEval_Modulation_Qhsl_P5Q_Average.rst
	Bluetooth_Measurement_MultiEval_Modulation_Qhsl_P5Q_Current.rst
	Bluetooth_Measurement_MultiEval_Modulation_Qhsl_P5Q_Maximum.rst
	Bluetooth_Measurement_MultiEval_Modulation_Qhsl_P5Q_StandardDev.rst