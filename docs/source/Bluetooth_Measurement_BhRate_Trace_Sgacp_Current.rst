Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:BHRate:TRACe:SGACp:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:BHRate:TRACe:SGACp:CURRent

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:BHRate:TRACe:SGACp:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:BHRate:TRACe:SGACp:CURRent



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.BhRate.Trace.Sgacp.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: