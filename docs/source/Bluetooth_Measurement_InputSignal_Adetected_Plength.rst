Plength
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.InputSignal.Adetected.Plength.PlengthCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.inputSignal.adetected.plength.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_InputSignal_Adetected_Plength_Brate.rst
	Bluetooth_Measurement_InputSignal_Adetected_Plength_Edrate.rst
	Bluetooth_Measurement_InputSignal_Adetected_Plength_LowEnergy.rst
	Bluetooth_Measurement_InputSignal_Adetected_Plength_Qhsl.rst