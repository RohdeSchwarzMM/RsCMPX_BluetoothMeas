Result
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDRP:RESult:PVTime
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDRP:RESult:SACP
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDRP:RESult:IQ
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDRP:RESult:EVMagnitude
	single: CONFigure:BLUetooth:MEASurement<Instance>:HDRP:RESult:TXSCalar

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:HDRP:RESult:PVTime
	CONFigure:BLUetooth:MEASurement<Instance>:HDRP:RESult:SACP
	CONFigure:BLUetooth:MEASurement<Instance>:HDRP:RESult:IQ
	CONFigure:BLUetooth:MEASurement<Instance>:HDRP:RESult:EVMagnitude
	CONFigure:BLUetooth:MEASurement<Instance>:HDRP:RESult:TXSCalar



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.Hdrp.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.bluetooth.measurement.hdrp.result.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Bluetooth_Measurement_Hdrp_Result_All.rst