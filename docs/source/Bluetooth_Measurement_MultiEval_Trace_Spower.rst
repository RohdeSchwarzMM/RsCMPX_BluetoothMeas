Spower
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Trace.Spower.SpowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.multiEval.trace.spower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_MultiEval_Trace_Spower_Average.rst
	Bluetooth_Measurement_MultiEval_Trace_Spower_Current.rst
	Bluetooth_Measurement_MultiEval_Trace_Spower_Maximum.rst
	Bluetooth_Measurement_MultiEval_Trace_Spower_Minimum.rst