Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:BHRate:PVTime:MINimum
	single: FETCh:BLUetooth:MEASurement<Instance>:BHRate:PVTime:MINimum
	single: CALCulate:BLUetooth:MEASurement<Instance>:BHRate:PVTime:MINimum

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:BHRate:PVTime:MINimum
	FETCh:BLUetooth:MEASurement<Instance>:BHRate:PVTime:MINimum
	CALCulate:BLUetooth:MEASurement<Instance>:BHRate:PVTime:MINimum



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.BhRate.PowerVsTime.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: