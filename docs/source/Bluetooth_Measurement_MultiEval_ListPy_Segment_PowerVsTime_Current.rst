Current
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:PVTime:CURRent

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:PVTime:CURRent



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.ListPy.Segment.PowerVsTime.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: