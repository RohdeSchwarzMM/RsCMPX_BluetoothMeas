Brate
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Modulation.Brate.BrateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.multiEval.modulation.brate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_MultiEval_Modulation_Brate_Average.rst
	Bluetooth_Measurement_MultiEval_Modulation_Brate_Current.rst
	Bluetooth_Measurement_MultiEval_Modulation_Brate_Maximum.rst
	Bluetooth_Measurement_MultiEval_Modulation_Brate_Minimum.rst
	Bluetooth_Measurement_MultiEval_Modulation_Brate_StandardDev.rst
	Bluetooth_Measurement_MultiEval_Modulation_Brate_Xmaximum.rst
	Bluetooth_Measurement_MultiEval_Modulation_Brate_Xminimum.rst
	Bluetooth_Measurement_MultiEval_Modulation_Brate_YieldPy.rst