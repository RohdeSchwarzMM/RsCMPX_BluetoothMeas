Nap
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:NAP:QHSL
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:NAP

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:NAP:QHSL
	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:NAP



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.InputSignal.Nap.NapCls
	:members:
	:undoc-members:
	:noindex: