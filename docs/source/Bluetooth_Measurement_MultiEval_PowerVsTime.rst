PowerVsTime
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.PowerVsTime.PowerVsTimeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.multiEval.powerVsTime.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_MultiEval_PowerVsTime_Brate.rst
	Bluetooth_Measurement_MultiEval_PowerVsTime_Edrate.rst
	Bluetooth_Measurement_MultiEval_PowerVsTime_LowEnergy.rst
	Bluetooth_Measurement_MultiEval_PowerVsTime_Nmode.rst
	Bluetooth_Measurement_MultiEval_PowerVsTime_Qhsl.rst