P2Q
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Modulation.Qhsl.P2Q.P2QCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.multiEval.modulation.qhsl.p2Q.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_MultiEval_Modulation_Qhsl_P2Q_Average.rst
	Bluetooth_Measurement_MultiEval_Modulation_Qhsl_P2Q_Current.rst
	Bluetooth_Measurement_MultiEval_Modulation_Qhsl_P2Q_Maximum.rst
	Bluetooth_Measurement_MultiEval_Modulation_Qhsl_P2Q_StandardDev.rst