Synchronise
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:BLUetooth:SYNChronise

.. code-block:: python

	DIAGnostic:BLUetooth:SYNChronise



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Diagnostic.Bluetooth.Synchronise.SynchroniseCls
	:members:
	:undoc-members:
	:noindex: