Fdeviation
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Trace.Fdeviation.FdeviationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.multiEval.trace.fdeviation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_MultiEval_Trace_Fdeviation_Average.rst
	Bluetooth_Measurement_MultiEval_Trace_Fdeviation_Current.rst
	Bluetooth_Measurement_MultiEval_Trace_Fdeviation_Maximum.rst
	Bluetooth_Measurement_MultiEval_Trace_Fdeviation_Minimum.rst