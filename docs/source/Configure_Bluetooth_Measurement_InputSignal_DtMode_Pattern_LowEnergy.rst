LowEnergy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:DTMode:PATTern:LENergy:LE1M
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:DTMode:PATTern:LENergy:LE2M
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:DTMode:PATTern:LENergy:LRANge

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:DTMode:PATTern:LENergy:LE1M
	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:DTMode:PATTern:LENergy:LE2M
	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:DTMode:PATTern:LENergy:LRANge



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.InputSignal.DtMode.Pattern.LowEnergy.LowEnergyCls
	:members:
	:undoc-members:
	:noindex: