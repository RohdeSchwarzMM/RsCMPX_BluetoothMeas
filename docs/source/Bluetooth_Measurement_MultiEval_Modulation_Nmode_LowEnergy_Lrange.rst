Lrange
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Modulation.Nmode.LowEnergy.Lrange.LrangeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.multiEval.modulation.nmode.lowEnergy.lrange.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_MultiEval_Modulation_Nmode_LowEnergy_Lrange_Average.rst
	Bluetooth_Measurement_MultiEval_Modulation_Nmode_LowEnergy_Lrange_Current.rst
	Bluetooth_Measurement_MultiEval_Modulation_Nmode_LowEnergy_Lrange_Maximum.rst
	Bluetooth_Measurement_MultiEval_Modulation_Nmode_LowEnergy_Lrange_Minimum.rst
	Bluetooth_Measurement_MultiEval_Modulation_Nmode_LowEnergy_Lrange_StandardDev.rst
	Bluetooth_Measurement_MultiEval_Modulation_Nmode_LowEnergy_Lrange_StDev.rst
	Bluetooth_Measurement_MultiEval_Modulation_Nmode_LowEnergy_Lrange_Xmaximum.rst
	Bluetooth_Measurement_MultiEval_Modulation_Nmode_LowEnergy_Lrange_Xminimum.rst