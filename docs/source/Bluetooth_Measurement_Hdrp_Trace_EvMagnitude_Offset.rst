Offset
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.Hdrp.Trace.EvMagnitude.Offset.OffsetCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.hdrp.trace.evMagnitude.offset.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_Hdrp_Trace_EvMagnitude_Offset_Average.rst
	Bluetooth_Measurement_Hdrp_Trace_EvMagnitude_Offset_Current.rst
	Bluetooth_Measurement_Hdrp_Trace_EvMagnitude_Offset_Maximum.rst