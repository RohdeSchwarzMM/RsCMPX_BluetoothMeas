Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:MINimum
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:MINimum
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:MINimum

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:MINimum
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:MINimum
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:BRATe:MINimum



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Modulation.Brate.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: