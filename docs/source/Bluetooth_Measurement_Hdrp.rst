Hdrp
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ABORt:BLUetooth:MEASurement<Instance>:HDRP
	single: STOP:BLUetooth:MEASurement<Instance>:HDRP
	single: INITiate:BLUetooth:MEASurement<Instance>:HDRP

.. code-block:: python

	ABORt:BLUetooth:MEASurement<Instance>:HDRP
	STOP:BLUetooth:MEASurement<Instance>:HDRP
	INITiate:BLUetooth:MEASurement<Instance>:HDRP



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.Hdrp.HdrpCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.hdrp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_Hdrp_InputSignal.rst
	Bluetooth_Measurement_Hdrp_Modulation.rst
	Bluetooth_Measurement_Hdrp_PowerVsTime.rst
	Bluetooth_Measurement_Hdrp_Sacp.rst
	Bluetooth_Measurement_Hdrp_State.rst
	Bluetooth_Measurement_Hdrp_Trace.rst