Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:BHRate:MODulation:MAXimum
	single: FETCh:BLUetooth:MEASurement<Instance>:BHRate:MODulation:MAXimum
	single: CALCulate:BLUetooth:MEASurement<Instance>:BHRate:MODulation:MAXimum

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:BHRate:MODulation:MAXimum
	FETCh:BLUetooth:MEASurement<Instance>:BHRate:MODulation:MAXimum
	CALCulate:BLUetooth:MEASurement<Instance>:BHRate:MODulation:MAXimum



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.BhRate.Modulation.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: