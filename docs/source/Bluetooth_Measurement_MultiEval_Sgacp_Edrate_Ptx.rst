Ptx
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:SGACp:EDRate[:PTX]
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:SGACp:EDRate[:PTX]
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:SGACp:EDRate[:PTX]

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:SGACp:EDRate[:PTX]
	READ:BLUetooth:MEASurement<Instance>:MEValuation:SGACp:EDRate[:PTX]
	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:SGACp:EDRate[:PTX]



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Sgacp.Edrate.Ptx.PtxCls
	:members:
	:undoc-members:
	:noindex: