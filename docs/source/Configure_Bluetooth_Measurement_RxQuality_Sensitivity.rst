Sensitivity
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:SENSitivity:STARtlevel
	single: CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:SENSitivity:STEPsize
	single: CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:SENSitivity:RETRy

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:SENSitivity:STARtlevel
	CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:SENSitivity:STEPsize
	CONFigure:BLUetooth:MEASurement<Instance>:RXQuality:SENSitivity:RETRy



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.RxQuality.Sensitivity.SensitivityCls
	:members:
	:undoc-members:
	:noindex: