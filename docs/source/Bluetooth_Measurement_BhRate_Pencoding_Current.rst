Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:BHRate:PENCoding:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:BHRate:PENCoding:CURRent
	single: CALCulate:BLUetooth:MEASurement<Instance>:BHRate:PENCoding:CURRent

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:BHRate:PENCoding:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:BHRate:PENCoding:CURRent
	CALCulate:BLUetooth:MEASurement<Instance>:BHRate:PENCoding:CURRent



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.BhRate.Pencoding.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: