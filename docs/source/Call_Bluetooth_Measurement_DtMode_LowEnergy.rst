LowEnergy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALL:BLUetooth:MEASurement<Instance>:DTMode:LENergy:RRESult
	single: CALL:BLUetooth:MEASurement<Instance>:DTMode:LENergy:RESet

.. code-block:: python

	CALL:BLUetooth:MEASurement<Instance>:DTMode:LENergy:RRESult
	CALL:BLUetooth:MEASurement<Instance>:DTMode:LENergy:RESet



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Call.Bluetooth.Measurement.DtMode.LowEnergy.LowEnergyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.call.bluetooth.measurement.dtMode.lowEnergy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Call_Bluetooth_Measurement_DtMode_LowEnergy_Rdevices.rst