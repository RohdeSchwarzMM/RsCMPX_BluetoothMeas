Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:BHRate:TRACe:DEVMagnitude:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:BHRate:TRACe:DEVMagnitude:CURRent

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:BHRate:TRACe:DEVMagnitude:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:BHRate:TRACe:DEVMagnitude:CURRent



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.BhRate.Trace.DevMagnitude.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: