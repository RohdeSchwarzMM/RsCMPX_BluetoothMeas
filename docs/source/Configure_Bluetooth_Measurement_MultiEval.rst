MultiEval
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:TOUT
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:MOEXception
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SCONdition
	single: CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:REPetition

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:TOUT
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:MOEXception
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:SCONdition
	CONFigure:BLUetooth:MEASurement<Instance>:MEValuation:REPetition



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.bluetooth.measurement.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Bluetooth_Measurement_MultiEval_Brate.rst
	Configure_Bluetooth_Measurement_MultiEval_Edrate.rst
	Configure_Bluetooth_Measurement_MultiEval_Frange.rst
	Configure_Bluetooth_Measurement_MultiEval_Limit.rst
	Configure_Bluetooth_Measurement_MultiEval_ListPy.rst
	Configure_Bluetooth_Measurement_MultiEval_LowEnergy.rst
	Configure_Bluetooth_Measurement_MultiEval_Malgorithm.rst
	Configure_Bluetooth_Measurement_MultiEval_Measurement.rst
	Configure_Bluetooth_Measurement_MultiEval_Qhsl.rst
	Configure_Bluetooth_Measurement_MultiEval_Result.rst
	Configure_Bluetooth_Measurement_MultiEval_Sacp.rst
	Configure_Bluetooth_Measurement_MultiEval_Scount.rst
	Configure_Bluetooth_Measurement_MultiEval_Sgacp.rst
	Configure_Bluetooth_Measurement_MultiEval_Synchronise.rst