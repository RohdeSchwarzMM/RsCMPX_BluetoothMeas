Sacp
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.MultiEval.Sacp.SacpCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.bluetooth.measurement.multiEval.sacp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Bluetooth_Measurement_MultiEval_Sacp_Brate.rst
	Configure_Bluetooth_Measurement_MultiEval_Sacp_LowEnergy.rst
	Configure_Bluetooth_Measurement_MultiEval_Sacp_Qhsl.rst