Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:CLASsic:CURRent
	single: FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:CLASsic:CURRent
	single: READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:CLASsic:CURRent

.. code-block:: python

	CALCulate:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:CLASsic:CURRent
	FETCh:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:CLASsic:CURRent
	READ:BLUetooth:MEASurement<Instance>:MEValuation:MODulation:NMODe:CLASsic:CURRent



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.MultiEval.Modulation.Nmode.Classic.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: