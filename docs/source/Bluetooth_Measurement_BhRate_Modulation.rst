Modulation
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.BhRate.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.bhRate.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_BhRate_Modulation_Average.rst
	Bluetooth_Measurement_BhRate_Modulation_Current.rst
	Bluetooth_Measurement_BhRate_Modulation_Maximum.rst
	Bluetooth_Measurement_BhRate_Modulation_StandardDev.rst