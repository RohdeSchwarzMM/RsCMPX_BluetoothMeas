Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:BLUetooth:MEASurement<Instance>:BHRate:TRACe:PVTime:AVERage
	single: FETCh:BLUetooth:MEASurement<Instance>:BHRate:TRACe:PVTime:AVERage

.. code-block:: python

	READ:BLUetooth:MEASurement<Instance>:BHRate:TRACe:PVTime:AVERage
	FETCh:BLUetooth:MEASurement<Instance>:BHRate:TRACe:PVTime:AVERage



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.BhRate.Trace.PowerVsTime.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: