All
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BLUetooth:MEASurement<Instance>:TRX:STATe:ALL

.. code-block:: python

	FETCh:BLUetooth:MEASurement<Instance>:TRX:STATe:ALL



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.Trx.State.All.AllCls
	:members:
	:undoc-members:
	:noindex: