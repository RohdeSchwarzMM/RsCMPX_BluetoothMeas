Dacc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:DACC:QHSL

.. code-block:: python

	CONFigure:BLUetooth:MEASurement<Instance>:ISIGnal:DACC:QHSL



.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Configure.Bluetooth.Measurement.InputSignal.Dacc.DaccCls
	:members:
	:undoc-members:
	:noindex: