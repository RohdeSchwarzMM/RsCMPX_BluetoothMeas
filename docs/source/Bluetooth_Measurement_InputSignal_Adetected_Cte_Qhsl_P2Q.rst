P2Q
----------------------------------------





.. autoclass:: RsCMPX_BluetoothMeas.Implementations.Bluetooth.Measurement.InputSignal.Adetected.Cte.Qhsl.P2Q.P2QCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bluetooth.measurement.inputSignal.adetected.cte.qhsl.p2Q.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bluetooth_Measurement_InputSignal_Adetected_Cte_Qhsl_P2Q_TypePy.rst
	Bluetooth_Measurement_InputSignal_Adetected_Cte_Qhsl_P2Q_Units.rst